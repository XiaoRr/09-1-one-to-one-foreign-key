package com.twuc.webApp.domain.oneToOne;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserProfileEntityRepository extends JpaRepository<UserProfileEntity, Long> {
}
